# estrategia-net

Gerenciando seus branches com o Git Flow

Fontes:
Gerenciando seus branches com o Git Flow <br>
https://tableless.com.br/git-flow-introducao/ <br>

Trabalhando com git e git-flow no dia a dia(básico) <br>
https://medium.com/diegowribeiro/trabalhando-com-git-e-git-flow-no-dia-a-dia-b%C3%A1sico-96a3ae02f8e3 <br>

Utilizando o fluxo Git Flow <br>
https://medium.com/trainingcenter/utilizando-o-fluxo-git-flow-e63d5e0d5e04 <br>

Tutorial: Git com Git Flow. <br>
https://medium.com/@lariodiniz/tutorial-git-com-git-flow-476ad906c8ae <br>
﻿using gerenciador.core.Entity;

namespace gerenciador.core.Inteface
{
    public interface IClienteRepository
    {
        Cliente ObterPorId(int idCliente);
        Cliente Salvar(Cliente cliente);
    }
}

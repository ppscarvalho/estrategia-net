﻿using gerenciador.core.Entity;

namespace gerenciador.core.Inteface
{
    public interface IProdutoRepository
    {
        Produto ObterPorId(int idProduto);
        Produto Salvar(Produto produto);
    }
}

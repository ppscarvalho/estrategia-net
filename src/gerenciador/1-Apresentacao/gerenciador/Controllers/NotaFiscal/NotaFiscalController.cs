﻿using Microsoft.AspNetCore.Mvc;

namespace gerenciador.Controllers.NotaFiscal
{
    public class NotaFiscalController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Edit()
        {
            return View();
        }

        public IActionResult Details()
        {
            return View();
        }

    }
}
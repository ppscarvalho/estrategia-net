﻿namespace gerenciador.ViewModel
{
    public class ProdutoViewModel
    {
        public int IdProduto { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
    }
}
